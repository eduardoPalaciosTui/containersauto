
object Global {

  val APP_NAME = "ContainersAuto"

  val USER_HOME_DIR: String = System.getProperty("user.home")
  val SLASH: String = System.getProperty("file.separator")

  val DOWNLOADS = s"/$USER_HOME_DIR/Downloads"
  val TDA_APP_ROUTE = s"/$USER_HOME_DIR/tda.android/"
  val TDA_SRC_APP_ROUTE = s"/$USER_HOME_DIR/tda.android/app/src"
  val CONTAINERS = "/assets/containers"

}