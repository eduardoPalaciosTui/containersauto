import java.io.File
import java.nio.file.{Files, Paths, StandardCopyOption}

import scala.util.Try

object Main {

  def main(args: Array[String]): Unit = {
    if(args.isEmpty) throw new IllegalArgumentException("Arguments are empty, you need to pass a ticket number such as LAND-1234")
    val ticket = args(0)
    if(!ticket.contains("LAND-") && !ticket.contains("TDA-")) throw new IllegalArgumentException("Invalid argument, you need to pass a ticket number such as LAND-1234 or TDA-1234")

    val src = getSrcDirectory(Global.TDA_SRC_APP_ROUTE)

    // find containers in downloads folder
    val newContainers = getNewContainersDir(ticket)
    if(newContainers.isEmpty) throw new IllegalArgumentException(s"Cannot find new containers with argument: $ticket , check Downloads folder to ensure we have a destination folder")
    // calculate environment type (dev or prod)
    val environment = getContainersEnvironment(newContainers)
    // rename containers
    renameNewContainers(newContainers)
    // fetch renamed containers from develop after renaming (rename file does not return a list hence we need to do this again)
    val newRenamedContainers = getNewContainersDir(ticket) // from downloads folder

    environment match {
      case Dev => copyContainers(findContainerJsonFiles(getDevContainersDirectories(src)), newRenamedContainers)
      case Prod => copyContainers(findContainerJsonFiles(getReleaseContainersDirectories(src)), newRenamedContainers)
    }
  }

  def getSrcDirectory(dir: String): List[File] = {
    val d = new File(dir)
    if (d.exists) {
      d.listFiles.filter(_.isDirectory).toList
    } else {
      throw new IllegalArgumentException("directory " + d.toString + "does not exist")
    }
  }

  def getDevContainersDirectories(sourceDirectories: List[File]): List[File] = {
    sourceDirectories
      .filter(_.isDirectory)
      .filter(_.getName.endsWith("Debug")) // includes minified folders
  }

  def getReleaseContainersDirectories(sourceDirectories: List[File]): List[File] = {
    sourceDirectories.filter(file => file.isDirectory && file.getName.endsWith("Release"))
  }

  def findContainerJsonFiles(files: List[File]): List[File] = {
    files.map(file => new File(s"${file.getPath}/${Global.CONTAINERS}")).filter(_.exists()).flatMap(_.listFiles())
  }

  def copyContainers(containersInProject: List[File], newContainers: List[File]): Unit = {
    val move = (source: File, target: File) => {
      Files.copy(
        Paths.get(source.getPath),
        Paths.get(target.getPath),
        StandardCopyOption.REPLACE_EXISTING
      )
    }

    for (newContainer <- newContainers) {
      containersInProject.filter(_.getName.equals(newContainer.getName)).foreach(move(newContainer, _))
    }
  }

  def getNewContainersDir(containersFolder: String): List[File] = {
    val downloads = new File(Global.DOWNLOADS)
    downloads.listFiles().toList.find(_.getName.equals(containersFolder)).get.listFiles().filter(_.getName.endsWith(".json")).toList
  }

  def getNewContainersNames(containers: List[File]): List[String] = {
    containers.map(file => file.getName.substring(file.getName.indexOf("G")))
  }

  def renameNewContainers(containers: List[File]): Unit = {
    containers.foreach(file => Try(file.renameTo(new File(file.getPath.replace(file.getPath.substring(file.getPath.lastIndexOf('/') + 1, file.getPath.indexOf("G")), "")))))
  }

  def getContainersEnvironment(containersFile: List[File]): Environment = {
    if (containersFile.forall(_.getName.contains("Dev"))) Dev else Prod
  }
}

sealed trait Environment

case object Dev extends Environment

case object Prod extends Environment
